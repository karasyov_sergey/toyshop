package ru.kkk.toyshop;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.kkk.toyshop.valid.AccountDataChecker;
import ru.kkk.toyshop.valid.CheckerType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private AccountDataChecker checker;

    @Before
    public void prepare() {
        AccountDataChecker.create();
        checker = AccountDataChecker.getChecker();
    }

    @Test
    public void doTrueEmailTest() {
        assertTrue(checker.isValid(null, CheckerType.EMAIL, "account@gmail.com"));
    }

    @Test
    public void doFalseEmailTest() {
        assertFalse(checker.isValid(null, CheckerType.EMAIL, "accountgmail.com"));
    }

    @Test
    public void doFalseEmailTestTwo() {
        assertFalse(checker.isValid(null, CheckerType.EMAIL, ""));
    }


    @Test
    public void doTrueNameTest() {
        assertTrue(checker.isValid(null, CheckerType.NAME, "account"));
    }

    @Test
    public void doFalseNameTestTwo() {
        assertFalse(checker.isValid(null, CheckerType.NAME, ""));
    }

    @Test
    public void doTruePasswordTest() {
        assertTrue(checker.isValid(null, CheckerType.PASSWORD, "1234", "1234", "account"));
    }

    @Test
    public void doFalsePasswordTestTwo() {
        assertFalse(checker.isValid(null, CheckerType.PASSWORD, "1234", "1234", "1234"));
    }

    @Test
    public void doFalsePasswordTestThree() {
        assertFalse(checker.isValid(null, CheckerType.PASSWORD, "1234", "1235", "account"));
    }

}