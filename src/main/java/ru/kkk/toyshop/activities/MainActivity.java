package ru.kkk.toyshop.activities;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.sql.SQLException;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.db.ToyDBContainer;
import ru.kkk.toyshop.helper.IConstants;
import ru.kkk.toyshop.helper.IDBConnectionData;
import ru.kkk.toyshop.ui.dialog.DialogueBox;
import ru.kkk.toyshop.valid.AccountDataChecker;

/**
 * Класс, представляющий собой главную активность
 * данного приложения
 *
 * @author KSO 17IT17
 */
public class MainActivity extends AppCompatActivity implements
        MenuItem.OnMenuItemClickListener, IConstants, IDBConnectionData {
    private NavController navController;
    private View header;
    private AppBarConfiguration mAppBarConfiguration;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.INTERNET},
                PackageManager.PERMISSION_GRANTED);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            ToyDBContainer.createCollection();
        } catch (ClassNotFoundException | SQLException e) {
            Toast.makeText(this, R.string.sql_error_conn, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        preferences = getSharedPreferences(KEY_PREFERENCES,
                MODE_PRIVATE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_shop, R.id.nav_basket, R.id.nav_authorization,
                R.id.nav_registration)
                .setDrawerLayout(drawer)
                .build();
        header = navigationView.getHeaderView(0);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        setHeaderName();
        AccountDataChecker.create();
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * Устанавливает отображаемый на
     * экране пользователя фрагмент
     *
     * @param resId идентификатор фрагмента
     */
    public void setFragment(int resId) {
        navController.navigate(resId);
    }

    /**
     * Устанавливает данные заголовка бокового меню, опираясь
     * на данные, хранящиеся в локальном хранилище приложения
     */
    public void setHeaderName() {
        TextView userDataTV = header.findViewById(R.id.user_data);
        String name = preferences.getString(KEY_NAME, NULL_VALUE);
        String email = preferences.getString(KEY_EMAIL, NULL_VALUE);
        if (name != null && email != null) {
            String header = getApplication().getString(R.string.nav_header_title);
            userDataTV.setText(String.format(header, name, email));
        }

    }

    /**
     * Очищат данные заголовка бокового меню
     */
    public void clearHeader() {
        TextView userDataTV = header.findViewById(R.id.user_data);
        userDataTV.setText(null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (preferences.contains(KEY_EMAIL)) {
            getMenuInflater().inflate(R.menu.log_out, menu);
            MenuItem item = menu.findItem(R.id.log_out);
            item.setOnMenuItemClickListener(this);
        }
        return true;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        DialogueBox dialogueBox = new DialogueBox(this);
        dialogueBox.show(getSupportFragmentManager(), null);
        return true;
    }
}
