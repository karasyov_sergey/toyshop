package ru.kkk.toyshop.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.db.SQLDBConnection;
import ru.kkk.toyshop.db.Toy;
import ru.kkk.toyshop.db.ToyDBContainer;
import ru.kkk.toyshop.helper.IConstants;

/**
 * Класс, представляющий собой активность, которая
 * отображает подробные данные о выбранной модели
 * автомобиля
 *
 * @author KSO 17IT17
 */
public class ToyActivity extends AppCompatActivity implements
        View.OnClickListener, IConstants {
    private SharedPreferences preferences;
    private Toy toy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toy_activity);
        preferences = getSharedPreferences(KEY_PREFERENCES, MODE_PRIVATE);

        toy = ToyDBContainer.getToyById(getIntent().getIntExtra(KEY_INTENT, 0));

        ImageView toyImage = findViewById(R.id.toy_image);
        TextView name = findViewById(R.id.toy_name);
        TextView producer = findViewById(R.id.toy_producer);
        TextView quantity = findViewById(R.id.toy_quantity);
        TextView age = findViewById(R.id.toy_age);
        TextView price = findViewById(R.id.toy_price);
        TextView city = findViewById(R.id.toy_city);
        Button buyBtn = findViewById(R.id.btn_to_basket);

        buyBtn.setOnClickListener(this);

        setImage(toyImage, toy.getImageBytes());
        setTextData(name, toy.getName());
        setTextData(producer, toy.getProducer().getName(), R.string.producer);
        setTextData(quantity, String.valueOf(toy.getQuantity()), R.string.quantity);
        setTextData(age, toy.getCategory() + "+", R.string.age);
        setTextData(price, String.valueOf(toy.getPrice()), R.string.price);
        setTextData(city, toy.getProducer().getCity().getName(), R.string.city);

    }

    private void setTextData(TextView textView, String string, int idRes) {
        String temp = getString(idRes);
        textView.setText(String.format(temp, string));
    }

    private void setTextData(TextView textView, String string) {
        textView.setText(string);
    }


    private void setImage(ImageView imageView, byte[] bytes) {
        Glide.with(getApplicationContext()).asBitmap().
                load(bytes).
                into(imageView);
    }

    @Override
    public void onClick(View v) {
        String email = preferences.getString(KEY_EMAIL, NULL_VALUE);
        if (toy.getQuantity() == 0){
            Toast.makeText(this, R.string.toy_quantity_is_empty, Toast.LENGTH_LONG).show();
            return;
        }
        int q = toy.getQuantity() - 1;
        if (email != null) {
            try (Connection connection = SQLDBConnection.getConnection()) {
                Statement statement = connection.createStatement();
                String insertOrder = String.format(Locale.ENGLISH, "EXEC insert_order %d, '%s'; ", toy.getId(), email);
                String updateToyQ = String.format(Locale.ENGLISH, "EXEC update_toy_quantity %d, %d;", toy.getId(), q);
                statement.executeUpdate(insertOrder + updateToyQ);
                toy.setQuantity(q);
                Toast.makeText(this, R.string.added, Toast.LENGTH_LONG).show();
            } catch (SQLException | ClassNotFoundException e) {
                Toast.makeText(this, R.string.sql_error_conn, Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            return;
        }
        Toast.makeText(this, R.string.authorize, Toast.LENGTH_LONG).show();

    }
}
