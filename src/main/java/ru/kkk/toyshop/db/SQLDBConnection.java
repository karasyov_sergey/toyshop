package ru.kkk.toyshop.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ru.kkk.toyshop.helper.IDBConnectionData;

public class SQLDBConnection implements IDBConnectionData {

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }
}
