package ru.kkk.toyshop.db;

import android.util.Log;

import androidx.annotation.Nullable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ru.kkk.toyshop.helper.IDBConnectionData;

public class ToyDBContainer implements IDBConnectionData {
    private static ArrayList<Toy> toys = new ArrayList<>();
    private static boolean isNull = true;

    public static void createCollection() throws SQLException, ClassNotFoundException {
        if (!isNull) {
            return;
        }
        try (Connection connection = SQLDBConnection.getConnection()) {
            ArrayList<City> cities = new ArrayList<>();
            ArrayList<Factory> factories = new ArrayList<>();
            Statement statement = connection.createStatement();
            initCitiesList(cities, statement);
            initFactoryList(factories, cities, statement);
            initToyList(factories, statement);
            isNull = true;
        }
    }

    private static void initToyList(ArrayList<Factory> factories, Statement statement) throws SQLException {
        ResultSet set = statement.executeQuery("EXEC select_toys;");
        while (set.next()) {
            Factory factory = getFactoryByName(factories, set.getString("producer"));
            Toy toy = new Toy(set.getInt("id"), set.getString("name"), factory);
            toy.setCategory(set.getInt("age"));
            toy.setPrice(set.getInt("price"));
            toy.setQuantity(set.getInt("quantity"));
            toy.setImageBytes(set.getBytes("file_data"));
            toys.add(toy);
        }
    }

    @Nullable
    private static Factory getFactoryByName(ArrayList<Factory> factories, String producerName) {
        for (Factory factory :
                factories) {
            if (factory.getName().equals(producerName)) {
                return factory;
            }
        }
        return null;
    }

    private static void initFactoryList(ArrayList<Factory> factories, ArrayList<City> cities, Statement statement) throws SQLException {
        ResultSet set = statement.executeQuery("EXEC select_factories;");
        while (set.next()) {
            City city = getCityByName(cities, set.getString("city"));
            Factory factory = new Factory(set.getInt("id"), set.getString("name"), city);
            factories.add(factory);
        }
    }

    @Nullable
    private static City getCityByName(ArrayList<City> cities, String cityName) {
        for (City city :
                cities) {
            if (city.getName().equals(cityName)) {
                return city;
            }
        }
        return null;
    }

    private static void initCitiesList(ArrayList<City> cities, Statement statement) throws SQLException {
        ResultSet set = statement.executeQuery("EXEC select_cities;");
        while (set.next()) {
            City city = new City(set.getString("city"));
            cities.add(city);
        }
    }

    public static List<Toy> getToyList() {
        return toys;
    }

    @Nullable
    public static Toy getToyById(int id) {
        for (Toy toy : toys) {
            if (toy.getId() == id) {
                return toy;
            }
        }
        return null;
    }
}
