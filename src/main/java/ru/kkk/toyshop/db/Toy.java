package ru.kkk.toyshop.db;

public class Toy {
    private byte[] imageBytes;
    private int id;
    private String name;
    private Factory producer;
    private int price;
    private int quantity;
    private int category;

    public Toy(int id, String name, Factory producer) {
        this.id = id;
        this.name = name;
        this.producer = producer;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Factory getProducer() {
        return producer;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getCategory() {
        return category;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }
}
