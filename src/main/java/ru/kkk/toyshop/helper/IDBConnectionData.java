package ru.kkk.toyshop.helper;

public interface IDBConnectionData {
    String IP = "192.168.0.104";
    String PORT = "49172";
    String DRIVER = "net.sourceforge.jtds.jdbc.Driver";
    String DB = "ToyDB";
    String USER = "test";
    String PASSWORD = "test";
    String URL = "jdbc:jtds:sqlserver://" + IP + ":" + PORT + "/" + DB;
}
