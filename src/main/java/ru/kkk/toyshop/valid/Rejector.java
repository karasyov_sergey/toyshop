package ru.kkk.toyshop.valid;

import android.content.Context;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatEditText;

public class Rejector implements IRejectObject {

    @Override
    public void reject(View view, int stringId) {
        if (view == null) {
            return;
        }
        if (view.getClass() != AppCompatEditText.class) {
            return;
        }
        ((EditText) view).setError(view.getContext().getString(stringId));
    }

    @Override
    public void reject(Context context, int stringId) {

    }
}
