package ru.kkk.toyshop.valid;

import android.content.Context;
import android.view.View;

public interface IRejectObject {
    void reject(View view, int stringId);

    void reject(Context context, int stringId);
}
