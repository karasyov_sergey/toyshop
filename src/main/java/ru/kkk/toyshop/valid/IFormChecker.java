package ru.kkk.toyshop.valid;

public interface IFormChecker {
    boolean isValid(String... data);
}
