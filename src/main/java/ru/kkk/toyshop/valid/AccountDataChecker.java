package ru.kkk.toyshop.valid;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.kkk.toyshop.R;

public class AccountDataChecker {

    private static AccountDataChecker checker;
    private static int rejectStringId;
    private HashMap<CheckerType, List<IFormChecker>> checkerMap;
    private Rejector rejector;

    public static void create() {
        if (checker != null) {
            return;
        }
        checker = new AccountDataChecker();
    }

    private AccountDataChecker() {
        rejector = new Rejector();
        checkerMap = new HashMap<>();
        checkerMap.put(CheckerType.EMAIL, createEmailChecker());
        checkerMap.put(CheckerType.PASSWORD, createPasswordChecker());
        checkerMap.put(CheckerType.NAME, createNameChecker());
    }

    private List<IFormChecker> createNameChecker() {
        List<IFormChecker> nameCheckers = new ArrayList<>();
        nameCheckers.add(data -> {
            rejectStringId = R.string.enter_name;
            return data[0] != null;
        });

        nameCheckers.add(data -> {
            rejectStringId = R.string.enter_name;
            return !data[0].isEmpty();
        });
        return nameCheckers;
    }

    private List<IFormChecker> createPasswordChecker() {
        List<IFormChecker> passwordCheckers = new ArrayList<>();
        passwordCheckers.add(data -> {
            rejectStringId = R.string.enter_password;
            return data[0] != null;
        });

        passwordCheckers.add(data -> {
            rejectStringId = R.string.enter_password;
            return !data[0].isEmpty();
        });

        passwordCheckers.add(data -> {
            rejectStringId = R.string.passwords_not_equals;
            return data[0].equals(data[1]);
        });

        passwordCheckers.add(data -> {
            rejectStringId = R.string.invalid_password_format;
            Pattern pattern = Pattern.compile("\\s+");
            Matcher matcher = pattern.matcher(data[0]);
            return !matcher.find();
        });

        passwordCheckers.add(data -> {
            rejectStringId = R.string.not_like_name;
            return !data[0].equals(data[2]);
        });
        return passwordCheckers;
    }

    private List<IFormChecker> createEmailChecker() {
        List<IFormChecker> emailCheckers = new ArrayList<>();
        emailCheckers.add(data -> {
            rejectStringId = R.string.enter_email;
            return data[0] != null;
        });
        emailCheckers.add(data -> {
            rejectStringId = R.string.enter_email;
            return !data[0].isEmpty();
        });
        emailCheckers.add(data -> {
            rejectStringId = R.string.invalid_email_format;
            return data[0].contains("@");
        });
        return emailCheckers;
    }

    public static AccountDataChecker getChecker() {
        return checker;
    }

    public boolean isValid(EditText view, CheckerType checkerType, String... data) {
        List<IFormChecker> checkers = checkerMap.get(checkerType);
        if (checkers == null) {
            return false;
        }
        for (IFormChecker aChecker : checkers) {
            if (!aChecker.isValid(data)) {
                rejector.reject(view, rejectStringId);
                return false;
            }
        }
        return true;
    }
}
