package ru.kkk.toyshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.activities.ToyActivity;
import ru.kkk.toyshop.db.Toy;
import ru.kkk.toyshop.db.ToyDBContainer;
import ru.kkk.toyshop.helper.IConstants;

/**
 * Класс, представляющий собой адаптер для интерфейсного
 * компонента RecyclerView и инициализирующий необходимые
 * данные для отображения автосалона
 *
 * @author KSO 17IT17
 */
public class ToyAdapter extends RecyclerView.Adapter<ToyAdapter.ToyHolder>
        implements Filterable, IConstants {
    private static final int FULL_TOY_KEY = 0;
    private static final int DYNAMIC_TOY_KEY = 1;
    private ArrayMap<Integer, List<Toy>> toyMap;
    private List<Toy> toys;
    private List<Toy> dynamicToys;
    private List<Integer> positionList;
    private Context context;
    private int currentKey;

    public ToyAdapter(List<Toy> toys, Context context) {
        this.context = context;
        toyMap = new ArrayMap<>();
        this.toys = toys;
        dynamicToys = new ArrayList<>();
        toyMap.put(FULL_TOY_KEY, toys);
        toyMap.put(DYNAMIC_TOY_KEY, dynamicToys);
        currentKey = FULL_TOY_KEY;
        positionList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ToyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.cell_toy, parent, false);
        return new ToyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ToyHolder holder, int position) {
        Toy toy = Objects.requireNonNull(toyMap.get(currentKey)).get(position);
        setImageFromBytes(toy.getImageBytes(), holder.img);
        setTextData(holder.name, toy.getName());
        setTextData(holder.producer, toy.getProducer().getName(), R.string.producer);
        setTextData(holder.price, String.valueOf(toy.getPrice()), R.string.price);
        holder.id = toy.getId();
    }

    /**
     * Изменяет содержимое текстового поля
     *
     * @param textView текстовое поле
     * @param string   строка, которую необходимо добавить
     * @param idRes    строковый ресурс, относительно которого форматируется строка
     */
    private void setTextData(TextView textView, String string, int idRes) {
        String temp = context.getString(idRes);
        textView.setText(String.format(temp, string));
    }

    private void setTextData(TextView textView, String string) {
        textView.setText(string);
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView при помощи библиотеки Glide
     *
     * @param bytes путь к изображению
     * @param image интерфейсный компонент ImageView
     */
    private void setImageFromBytes(byte[] bytes, ImageView image) {
        Glide.with(context).asBitmap().
                load(bytes).
                into(image);
    }


    @Override
    public int getItemCount() {
        return Objects.requireNonNull(toyMap.get(currentKey)).size();
    }

    @Override
    public Filter getFilter() {
        return carFilter;
    }

    private Filter carFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Objects.requireNonNull(dynamicToys).clear();
            if (constraint == null || constraint.length() == 0) {
                currentKey = FULL_TOY_KEY;
                return null;
            }
            for (Toy toy : toys) {
                if (toy.getName().toLowerCase().trim().contains(
                        constraint.toString().toLowerCase().trim())) {
                    dynamicToys.add(toy);
                }
            }
            currentKey = DYNAMIC_TOY_KEY;

            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    };

    /**
     * Вложенный класс, представляющий собой отдельную
     * ячейку, которая отображает необходимые данные
     * автомобиля
     *
     * @author KSO 17IT17
     */
    public class ToyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Integer id;
        private TextView price;
        private TextView name;
        private ImageView img;
        private TextView producer;

        ToyHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.toy_image);
            producer = itemView.findViewById(R.id.toy_producer);
            name = itemView.findViewById(R.id.toy_name);
            price = itemView.findViewById(R.id.toy_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ToyActivity.class);
            intent.putExtra(KEY_INTENT, id);
            context.startActivity(intent);
        }
    }
}