package ru.kkk.toyshop.ui.registration;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.activities.MainActivity;
import ru.kkk.toyshop.db.SQLDBConnection;
import ru.kkk.toyshop.valid.AccountDataChecker;
import ru.kkk.toyshop.valid.CheckerType;


public class RegistrationFragment extends Fragment implements View.OnClickListener {
    private EditText inputEmail;
    private EditText inputName;
    private EditText inputPassword;
    private EditText inputPasswordRepeat;
    private AccountDataChecker checker;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_registration,
                container, false);
        initialize(root);
        checker = AccountDataChecker.getChecker();
        return root;
    }

    private void initialize(View root) {
        inputEmail = root.findViewById(R.id.inputEmail);
        inputName = root.findViewById(R.id.inputName);
        inputPassword = root.findViewById(R.id.inputPassword);
        inputPasswordRepeat = root.findViewById(R.id.inputPasswordRepeat);
        Button button = root.findViewById(R.id.btnRegistration);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = inputEmail.getText().toString();
        String name = inputName.getText().toString().trim();
        String password = inputPassword.getText().toString();
        String passwordRepeat = inputPasswordRepeat.getText().toString();
        if (isValidData(email, name, password, passwordRepeat)) {
            addAccount(name, email, password);
        }

    }

    private void addAccount(String name, String email, String password) {
        try (Connection connection = SQLDBConnection.getConnection()) {
            Statement statement = connection.createStatement();
            if (!isFreeAcc(email, statement)) {
                Toast.makeText(getContext(), R.string.failed_reg, Toast.LENGTH_LONG).show();
                return;
            }
            statement.executeUpdate(String.format(Locale.ENGLISH, "EXEC insert_acc '%s', '%s', '%s';", name, email, password));
            Toast.makeText(getContext(), R.string.successful_reg, Toast.LENGTH_LONG).show();
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) {
                activity.setFragment(R.id.nav_authorization);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), R.string.sql_error_conn, Toast.LENGTH_LONG).show();
        }
    }


    private boolean isFreeAcc(String email, Statement statement) throws SQLException {
        ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH, "EXEC contains_acc '%s'", email));
        return !set.next();
    }


    private boolean isValidData(String email, String name, String password, String passwordRepeat) {
        if (!checker.isValid(inputEmail, CheckerType.EMAIL, email)) {
            return false;
        }
        if (!checker.isValid(inputPassword, CheckerType.PASSWORD, password, passwordRepeat, name)) {
            return false;
        }
        return checker.isValid(inputName, CheckerType.NAME, name);
    }
}