package ru.kkk.toyshop.ui.shop;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.adapter.ToyAdapter;
import ru.kkk.toyshop.db.ToyDBContainer;

/**
 * Класс, представляющий собой фрагмент, отображающий
 * интерфейсные компоненты для автосалона
 *
 * @author KSO 17IT17
 */
public class ShopFragment extends Fragment implements SearchView.OnQueryTextListener {
    private ToyAdapter toyAdapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_shop, container, false);
        initCarList(root);
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Инициализирует интерфейсные компоненты
     * для автосалона
     *
     * @param root базовый интерфейсный блок
     */
    private void initCarList(View root) {
        RecyclerView recyclerView = root.findViewById(R.id.shop_list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(layoutManager);
        toyAdapter = new ToyAdapter(ToyDBContainer.getToyList(), root.getContext());
        recyclerView.setAdapter(toyAdapter);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem menuItem = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        toyAdapter.getFilter().filter(newText);
        return false;
    }
}