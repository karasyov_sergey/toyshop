package ru.kkk.toyshop.ui.authorization;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.activities.MainActivity;
import ru.kkk.toyshop.db.SQLDBConnection;
import ru.kkk.toyshop.helper.IConstants;

/**
 * Класс, представляющий собой фрагмент, отображающий
 * необходимые интерфейсные компоненты для авторизации
 * зарегистрированного пользователя
 *
 * @author KSO 17IT17
 */
public class AuthorizationFragment extends Fragment implements
        View.OnClickListener, IConstants {

    private EditText inputEmail;
    private EditText inputPassword;
    private String userName;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_authorization,
                container, false);
        initialize(root);
        return root;
    }

    /**
     * Инициализирует интерфейсные компоненты для
     * текущего фрагмента
     *
     * @param root базовый интерфейсный блок
     */
    private void initialize(View root) {
        inputEmail = root.findViewById(R.id.inputEmail);
        inputPassword = root.findViewById(R.id.inputPassword);
        Button button = root.findViewById(R.id.btnAuthorization);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();
        if (email.isEmpty()) {
            inputEmail.setError(getString(R.string.enter_email));
            return;
        }
        if (password.isEmpty()) {
            inputPassword.setError(getString(R.string.enter_password));
            return;
        }

        if (isSuccess(email, password)) {
            Toast.makeText(v.getContext(),
                    getString(R.string.successful_authorization),
                    Toast.LENGTH_LONG).show();
            inputEmail.setText(null);
            inputPassword.setText(null);
            SharedPreferences sp = v.getContext().
                    getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(KEY_NAME, userName);
            editor.putString(KEY_EMAIL, email);
            editor.apply();

            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) {
                activity.setHeaderName();
                activity.setFragment(R.id.nav_shop);
            }

        } else {
            inputEmail.setError(getString(R.string.invalid_email));
            inputPassword.setError(getString(R.string.invalid_password));
        }
    }

    /**
     * Проверяет, корректные ли данные ввел пользователь
     * своего аккаунта
     *
     * @param email    эл. почта зарегистрированного пользователя
     * @param password пароль зарегистрированного пользователя
     * @return true если пользователь ввел данные своего аккаунта верно иначе false
     */
    private boolean isSuccess(String email, String password) {
        boolean bool = false;
        try (Connection connection = SQLDBConnection.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH, "EXEC contains_acc '%s', '%s';", email, password));
            if (set.next()) {
                userName = set.getString("login");
                bool = true;
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.sql_error_conn), Toast.LENGTH_LONG).show();
        }
        return bool;
    }
}