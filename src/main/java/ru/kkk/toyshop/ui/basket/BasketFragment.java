package ru.kkk.toyshop.ui.basket;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.kkk.toyshop.R;
import ru.kkk.toyshop.adapter.ToyAdapter;
import ru.kkk.toyshop.db.SQLDBConnection;
import ru.kkk.toyshop.db.Toy;
import ru.kkk.toyshop.db.ToyDBContainer;
import ru.kkk.toyshop.helper.IConstants;


/**
 * Класс, представляющий собой фрагмент, отображает
 * корзину на экране пользователя
 *
 * @author KSO 17IT17
 */
public class BasketFragment extends Fragment implements IConstants {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_basket, container, false);
        initBasket(root);

        return root;
    }

    /**
     * Инициализирует необходимые интерфейсные компоненты
     * корзины авторизованного пользователя
     *
     * @param root базовый интерфейсный блок
     */
    private void initBasket(View root) {
        String email = root.getContext().getSharedPreferences(KEY_PREFERENCES,
                Context.MODE_PRIVATE).getString(KEY_EMAIL, NULL_VALUE);
        if (email != null) {
            List<Toy> orders = getOrderList(email);
            RecyclerView recyclerView = root.findViewById(R.id.basket);
            LinearLayoutManager manager = new LinearLayoutManager(root.getContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(manager);
            ToyAdapter toyAdapter = new ToyAdapter(orders, root.getContext());
            recyclerView.setAdapter(toyAdapter);
        }
    }

    /**
     * Инициализирует данные для последующего отображения
     * в корзине авторизованного пользователя
     *
     * @param email эл. почта авторизованного пользователя
     */
    private List<Toy> getOrderList(String email) {
        ArrayList<Toy> orders = new ArrayList<>();
        try (Connection connection = SQLDBConnection.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH, "EXEC get_orders '%s'", email));
            while (set.next()) {
                int id = set.getInt("toy_id");
                Toy toy = ToyDBContainer.getToyById(id);
                orders.add(toy);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), R.string.sql_error_conn, Toast.LENGTH_LONG).show();
        }
        return orders;
    }

}